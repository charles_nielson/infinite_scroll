// Copyright (c) 2017, Charles. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'dart:html';
import 'package:angular2/core.dart';
import 'package:angular_components/angular_components.dart';

import 'hello_dialog/hello_dialog.dart';

@Component(
  selector: 'my-app',
  styleUrls: const ['app_component.css'],
  templateUrl: 'app_component.html',
  directives: const [materialDirectives, HelloDialog],
  providers: const [materialProviders],
)
class AppComponent {
  String get image => "images/Language-Translation1.jpg";
  String get image2 => "images/pacman.jpg";
  String get image3 => "images/xwing.jpg";
  List fullitems = [];
  List items = [];
  int verticalScroll;
  int elementHeight;
  int loadAmount = 50;
  int changeAmount = 2;
  int startAmount = 0;
  int maxAmount = 50;
  bool showScrollToTopButton = false;

  AppComponent() {
    for(var i = 0; i < 2000000; i++) {
      fullitems.add(
          {
            "index":i,
            "source":"This is the source sentence",
            "target":"Esta es la frase de la fuente."}
          );
    }
    for(var i = 0; i < loadAmount; i++) {
      items.add(fullitems[i]);
    }
  }

  bool isElementOutViewport (DivElement el) {
    var rect = el.getBoundingClientRect();
    return rect.bottom < 0 || rect.right < 0 || rect.left > window.innerWidth || rect.top > window.innerHeight;
  }

  bool checkLastElement() {
    String last = (loadAmount - 1).toString();
    DivElement lastElement = querySelector('#el$last');
    return isElementOutViewport(lastElement);
  }

  bool checkFirstElement() {
    String first = (loadAmount - maxAmount).toString();
    DivElement firstElement = querySelector('#el$first');
    return isElementOutViewport(firstElement);
  }

  showVerticalScroll() {
    String secondtolast = (loadAmount - 4).toString();
    print("second to last = $secondtolast");
    String secondtofirst = ((loadAmount - maxAmount) + 4).toString();
    DivElement lastElement = querySelector('#el$secondtolast');
    DivElement firstElement = querySelector('#el$secondtofirst');
    if(int.parse(secondtofirst) > 0) {
      showScrollToTopButton = true;
    }
    if(!checkLastElement()) {
      loadAmount += changeAmount;
      if(loadAmount > fullitems.length) {
        loadAmount = fullitems.length;
      }
      if(loadAmount > maxAmount) {
        startAmount = loadAmount - maxAmount;
      }
      items.clear();
      for(var i = startAmount; i < loadAmount; i++) {
        if(i < fullitems.length) {
          items.add(fullitems[i]);
        }
      }
      if(loadAmount < fullitems.length) {
        lastElement.scrollIntoView(ScrollAlignment.BOTTOM);
      }
    }
    if(loadAmount >= maxAmount) {
      if(!checkFirstElement()) {
        if(startAmount > 0) {
          startAmount -= changeAmount;
        } else {
          startAmount = 0;
        }
        loadAmount = startAmount + maxAmount;
        items.clear();
        for(var i = startAmount; i < loadAmount; i++) {
            items.add(fullitems[i]);
        }
        if(int.parse(secondtofirst) > 4) {
          firstElement.scrollIntoView(ScrollAlignment.TOP);
        }
      }
    }
  }

  jumpTo(String item) {
    startAmount = int.parse(item);
    if(startAmount > (fullitems.length - 1) - changeAmount) {
      startAmount = (fullitems.length - 1) - changeAmount;
    }
    loadAmount = startAmount + maxAmount;
    items.clear();
    for(var i = startAmount; i < loadAmount; i++) {
      items.add(fullitems[i]);
    }
    /*String itemplus = (int.parse(item) + 1).toString();
    querySelector('#e$itemplus').scrollIntoView(ScrollAlignment.CENTER);*/
  }

  scrollToTop() {
    jumpTo("0");
    querySelector('#el0').scrollIntoView(ScrollAlignment.TOP);
  }

  scrollCenter(InputElement target) {
    target.scrollIntoView(ScrollAlignment.CENTER);
  }

  catchKeyEvent(KeyboardEvent event, int index) {
    int next;
    if(event.keyCode == 40 || event.keyCode == 9) {
      event.preventDefault();
      event.stopPropagation();
      print("down $index");
      next = index + 1;
      if(next > fullitems.length - 1) {
        next = fullitems.length - 1;
      }
    }
    if(event.keyCode == 38) {
      event.preventDefault();
      event.stopPropagation();
      print("up $index");
      next = index - 1;
      if(next < 0) {
        next = 0;
      }
    }
    InputElement nextElement = querySelector('#input$next');
    nextElement.focus();
    nextElement.scrollIntoView(ScrollAlignment.CENTER);
  }
}
